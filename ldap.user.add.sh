#!/bin/bash

SUFFIX='dc=srv,dc=world' #variable that is dc entries
LDIF='ldapuser.ldif' #variable that is a ldif file for users

echo -n > $LDIF #add the following to the ldapuser.ldif file
GROUP_IDS=() #creating an array named GROUP_IDS
grep "x:[1-9][0-9][0-9][0-9]:" /etc/passwd | (while read TARGET_USER #take lines from /etc/passwd that have UID higher than 1000, i.e. users and not daemons | and while apply the value fro grep to the TARGET_USER variable create following variables from it:
do
    USER_ID="$(echo "$TARGET_USER" | cut -d':' -f1)" #UID is the first field in each line after the : delimeter from the TARGET_USER

    USER_NAME="$(echo "$TARGET_USER" | cut -d':' -f5 | cut -d' ' -f1,2)" # USER_NAME variable is declared from first two fields from the fifth field. i.e. full user name
    [ ! "$USER_NAME" ] && USER_NAME="$USER_ID" #if there is none use UID

    LDAP_SN="$(echo "$USER_NAME" | cut -d' ' -f2)" #last name from the full name of the user
    [ ! "$LDAP_SN" ] && LDAP_SN="$USER_NAME" #if there is none, use USER_NAME

    LASTCHANGE_FLAG="$(grep "${USER_ID}:" /etc/shadow | cut -d':' -f3)" # the warning period before user has to change his password value for each UID applies to variable
    [ ! "$LASTCHANGE_FLAG" ] && LASTCHANGE_FLAG="0" #if not set, set it to 0

    SHADOW_FLAG="$(grep "${USER_ID}:" /etc/shadow | cut -d':' -f9)" #WHAT DID YOU DO? THIS FIELD IS USUALLY UNUSED
    [ ! "$SHADOW_FLAG" ] && SHADOW_FLAG="0" #set it to 0 anyway

    GROUP_ID="$(echo "$TARGET_USER" | cut -d':' -f4)" #combining everything to a nice array for GROUP_ID
    [ ! "$(echo "${GROUP_IDS[@]}" | grep "$GROUP_ID")" ] && GROUP_IDS=("${GROUP_IDS[@]}" "$GROUP_ID") #if the value of the array GROUP_ID does not contain any entry, add new entries for each user
#the following lines add entries to the ldapusers.ldif file    
    echo "dn: uid=$USER_ID,ou=People,$SUFFIX" >> $LDIF #distinguished name is UID, belongs to People class, domanin components are srv.world
    echo "objectClass: inetOrgPerson" >> $LDIF #creating object class for organizational person for all users
    echo "objectClass: posixAccount" >> $LDIF #creating object class for all accounts
    echo "objectClass: shadowAccount" >> $LDIF #DON'T NOW WHAT THIS MEANS
    echo "sn: $LDAP_SN" >> $LDIF #adding the last name for the user
    echo "givenName: $(echo "$USER_NAME" | awk '{print $1}')" >> $LDIF #adding first name for the user from the first word of the variable USER_NAME
    echo "cn: $USER_NAME" >> $LDIF #common name is the USER_NAME
    echo "displayName: $USER_NAME" >> $LDIF #the name displayed is USER_NAME
    echo "uidNumber: $(echo "$TARGET_USER" | cut -d':' -f3)" >> $LDIF #the user ID from /etc/passwd
    echo "gidNumber: $(echo "$TARGET_USER" | cut -d':' -f4)" >> $LDIF #group ID from /etc/passwd
    echo "userPassword: {crypt}$(grep "${USER_ID}:" /etc/shadow | cut -d':' -f2)" >> $LDIF #encrypted password from /etc/shadow
    echo "gecos: $USER_NAME" >> $LDIF #users' full name
    echo "loginShell: $(echo "$TARGET_USER" | cut -d':' -f7)" >> $LDIF #working shell for each user from /etc/passwd
    echo "homeDirectory: $(echo "$TARGET_USER" | cut -d':' -f6)" >> $LDIF #home directory for each user from /etc/passwd
    echo "shadowExpire: $(passwd -S "$USER_ID" | awk '{print $7}')" >> $LDIF #when the password will expire, value from seventh field in /etc/shadow
    echo "shadowFlag: $SHADOW_FLAG" >> $LDIF #SAME COMMENT AS FOR SETTING THE VARIABLE  
    echo "shadowWarning: $(passwd -S "$USER_ID" | awk '{print $6}')" >> $LDIF #short information about the status of the password for a user, entries only the warning period
    echo "shadowMin: $(passwd -S "$USER_ID" | awk '{print $4}')" >> $LDIF #same as the previous line, entries only the minimum age
    echo "shadowMax: $(passwd -S "$USER_ID" | awk '{print $5}')" >> $LDIF #same, the maximum age
    echo "shadowLastChange: $LASTCHANGE_FLAG" >> $LDIF #the value of the warning period before changing the password, in this case always 0
    echo >> $LDIF #print each line one after another, in concatenation
done #closing the while loop

for TARGET_GROUP_ID in "${GROUP_IDS[@]}" #for each item, TARGET_GROUP_ID, in the GROUP_IDS array do 
do
    LDAP_CN="$(grep ":${TARGET_GROUP_ID}:" /etc/group | cut -d':' -f1)" #declaring the item from the cut command  and create an entry for the common name with group ID from /etc/group

    echo "dn: cn=$LDAP_CN,ou=Group,$SUFFIX" >> $LDIF #concatenating the entry for group ID 
    echo "objectClass: posixGroup" >> $LDIF #creating object class for each group
    echo "cn: $LDAP_CN" >> $LDIF #creating the common name for each group
    echo "gidNumber: $TARGET_GROUP_ID" >> $LDIF #adding the GUI for each group

    for MEMBER_UID in $(grep ":${TARGET_GROUP_ID}:" /etc/passwd | cut -d':' -f1,3) #for another item, MEMBER_ID, from the cut command from /etc/passwd - user name and UID-  in the TARGET_GROUP_ID array do
    do
        UID_NUM=$(echo "$MEMBER_UID" | cut -d':' -f2) #declaring the value for UID_NUM from second field of MEMBER_ID variable which is UID
        [ $UID_NUM -ge 1000 -a $UID_NUM -le 9999 ] && echo "memberUid: $(echo "$MEMBER_UID" | cut -d':' -f1)" >> $LDIF #check if the UID is greater than 1000, if yes add it to LDIF file with memberUid entry
    done #close the second for loop
    echo >> $LDIF #add line to LDIF file
done #close the first for loop
) #close the while value from the grep
